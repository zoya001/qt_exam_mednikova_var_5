import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12

Page {
    header: Rectangle {
        id:page2_header
        color: "#e8fffd"
        height: 40

        Button {
           id: menu
           width: 45
           height: 38
           anchors.verticalCenter: page2_header.verticalCenter
           icon.source: "images/menu.png"
           anchors.leftMargin: 20
        }

        Label {
            text: qsTr("Записи")
            font.pixelSize : 25
            anchors.verticalCenter: page2_header.verticalCenter
            anchors.left: menu.right
            anchors.leftMargin: 20
        }
    }

    ColumnLayout {
        Rectangle {
            color: "#6C8CD5"
            width: parent.width
            height: 150
            GridLayout {
                anchors.fill: parent
                columns: 3
                rows: 3
                Image {
                    id: img
                    source: "images/apple_music.png"
                    Layout.column: 0
                    Layout.row: 0
                    Layout.rowSpan: 3
                    Layout.preferredHeight: 130
                    Layout.preferredWidth: 130
                    Layout.margins: 5
                    fillMode: Image.PreserveAspectFit
                    property bool adapt: true
                }
               Label {
                   text: "Название"
                   Layout.topMargin: 20
                   Layout.column: 1
                   Layout.row: 0
                   Layout.fillHeight: true
                   Layout.preferredWidth: 150
                   font.pointSize: 11
                   anchors.leftMargin: 15
               }

               Label {
                   text: "Автор"
                   Layout.column: 1
                   Layout.row: 1
                   Layout.fillHeight: true
                   Layout.preferredWidth: 150
                   font.pointSize: 11
                   anchors.leftMargin: 15
               }

               Label {
                   text: "Время "
                   Layout.column: 2
                   Layout.row: 1
                   Layout.rowSpan: 3
                   Layout.fillHeight: true
                   Layout.preferredWidth: 150
                   font.pointSize: 11
                   anchors.leftMargin: 1
               }
           }
        }
    }
}
