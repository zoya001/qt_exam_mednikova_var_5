import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtMultimedia 5.15
import QtQuick.Controls.Universal 2.12


Page {
    id:page2
    header: Rectangle {
        id:page2_header
        color: "#e8fffd"
        height: 40

        Button {
           id: arrow
           width: 45
           height: 38
           anchors.verticalCenter: page2_header.verticalCenter
           icon.source: "images/back.png"
           anchors.leftMargin: 20
        }

        Label {
            text: qsTr("Воспроизведение")
            font.pixelSize : 25
            anchors.verticalCenter: page2_header.verticalCenter
            anchors.left: arrow.right
            anchors.leftMargin: 20
        }
    }

    ColumnLayout {
        anchors.fill: parent
        Layout.alignment: Qt.AlignCenter

            Image {
                Layout.alignment: Qt.AlignCenter
                id: picture1
                source: "images/apple_music.png"
                Layout.maximumWidth: parent.width/2
                Layout.maximumHeight: parent.width/2
                fillMode: Image.PreserveAspectFit
                smooth: true
                visible: true
            }

            MediaPlayer {
                id: running_audio
                source: "audio/loneliness.mp3"
            }

            Audio {
                id: playMusic
                source: running_audio
            }

            MouseArea {
                id: playArea
                anchors.fill: parent
                onPressed:  { playMusic.play() }
            }

        RowLayout {
            Layout.alignment: Qt.AlignCenter
            Layout.fillWidth: true
            Text {
                id: durationAudio
                font.pointSize: 10
                text: Qt.formatTime(new Date(running_audio.duration), "mm:ss")
            }
            Slider {
                id: audio_slider
                to: running_audio.duration
                value: running_audio.position
                Layout.fillWidth: true
                Universal.accent: "#6C8CD5"
                onPressedChanged: {
                running_audio.seek(audio_slider.value)
                }
            }

            Text {
                id: timer_video_num
                anchors.right: parent.right
                anchors.rightMargin: 10
                font.pointSize: 10
                text: Qt.formatTime(new Date(running_audio.position), "mm:ss")
            }
        }

        RowLayout {
            Layout.alignment: Qt.AlignCenter
            Layout.fillWidth: true
            Button {
                id: random
                icon.source: "images/tudasuda.png"
            }

            Button {
                id: backAudio
                icon.source: "images/back_audio.png"
            }

            Button {
                id: start_stop
                icon.source: {
                    if (MediaPlayer.PlayingState == running_audio.playbackState) {
                        return "images/pause.png"
                    }
                    else {
                        return "images/play.png"
                    }
                }
                onClicked: {
                    if (running_audio.playbackState == MediaPlayer.PlayingState) {
                        return running_audio.pause();
                    }
                    else {
                        return running_audio.play();
                    }
                }
            }

            Button {
                id: nextAudio
                icon.source: "images/foward_audio.png"
            }

            Button {
                id: cicle
                icon.source: "images/while.png"
                onClicked: {
                    //return "images/while1.png";
                    loops: MediaPlayer.Infinite // бесконечное повторение
                }
            }
        }
    }
}
