import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12

ApplicationWindow {
    width: 640
    height: 520
    visible: true
    title: qsTr("Music Player")

    SwipeView {
        id: swipeView
        anchors.fill: parent

        Page1Form {
        }

        Page2Form {
        }
    }

    Drawer {
        id: drawer
        width: 0.6 * parent.width
        height: parent.height

        Rectangle {
            anchors.fill: parent
            color: "#06266F"
        }

        ColumnLayout {
            anchors.margins: 10
            width: parent.width
            RowLayout {
                Layout.alignment: Qt.AlignCenter
                Label {
                    text: "Проигрыватель"
                    font.pixelSize: 25
                    color: "white"
                }
            }

            RowLayout {
                Layout.row: 1
                Layout.alignment: Qt.AlignCenter
                Label {
                    color: "#6C8CD5"
                    id: text_ex
                    text: "Экзаменационное задание по дисцилине\n    Разработка безопасных мобильных\n                       приложений,\n   Московский Политех, 16 июля, 2021 г "
                    font.pixelSize: 18
                }
            }

                RowLayout {
                   //width: 340
                    //height: 340
                    Layout.row: 2
                    anchors.margins: 10
                    Layout.alignment: Qt.AlignCenter
                        Image {
                            Layout.maximumWidth: 300
                            Layout.maximumHeight: 300
                            id: logoPolytech
                            source: "images/logo.png"
                    }
                }

                RowLayout {
                    Layout.row: 3
                    Layout.alignment: Qt.AlignLeft
                    Label {
                        id: text_auth
                        text: "Автор: z.mednikova01@gmail.com"
                        font.pixelSize: 17
                        color: "#6C8CD5"
                    }
                }

                RowLayout {
                    Layout.row: 3
                    Layout.alignment: Qt.AlignLeft
                    Text {
                        color: "#6C8CD5"
                        text: '<html><style type="text/css"></style><a href="https://gitlab.com/zoya001/qt_exam_mednikova_var_5">Git-репозиторий</a></html>'
                        font.pixelSize: 17
                        onLinkActivated: Qt.openUrlExternally(link)
                    }
                }
            }
        }
    }
